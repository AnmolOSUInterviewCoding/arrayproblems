/**
 * Created by Anmol on 2/22/2016.
 */
public class NQueen {

    public static void main(String[] args) {
        int n = 8;
        int nQueen[][] = new int[n][n];

        nQueenSolution(nQueen ,0 );
        for(int i = 0;i<nQueen.length;i++) {
            for(int j =0;j<nQueen[0].length;j++) {
                System.out.print(nQueen[i][j] +" ");
              }
            System.out.println();
        }
    }

    private static boolean nQueenSolution(int[][] nQueen, int col) {

        if(col>=nQueen[0].length)return true;

        for(int i = 0;i<nQueen.length;i++) {
            if(isSafe(nQueen, i, col)) {
                nQueen[i][col]=1;
                if(nQueenSolution(nQueen, col+1)) return true;

                nQueen[i][col] = 0;
            }
        }
        return false;

    }

    private static boolean isSafe(int[][] nQueen, int row, int col) {
        int i, j;
        /* Check this row on left side */
        for (i = 0; i < col; i++)
            if (nQueen[row][i] == 1)
                return false;

    /* Check upper diagonal on left side */
        for (i=row, j=col; i>=0 && j>=0; i--, j--)
            if (nQueen[i][j]==1)
                return false;

    /* Check lower diagonal on left side */
        for (i=row, j=col; j>=0 && i<nQueen[0].length; i++, j--)
            if (nQueen[i][j]==1)
                return false;

        return true;
    }
}

/**
 * Created by Anmol on 1/16/2016.
 */
public class LinkedListProblems {
    int val;
    LinkedListProblems next;

    public LinkedListProblems(int val) {
        this.val = val;
    }

    //    [2,5,3,4,6,2,2]
    public static void main(String[] args) {
        LinkedListProblems head = new LinkedListProblems(1);
        head.next = new LinkedListProblems(2);
//        head.next.next = null;
        head.next.next = new LinkedListProblems(3);
        head.next.next.next = new LinkedListProblems(4);
        head.next.next.next.next = new LinkedListProblems(5);
        head.next.next.next.next.next = new LinkedListProblems(6);
        head.next.next.next.next.next.next = new LinkedListProblems(7);
        head.next.next.next.next.next.next.next = null;
//        swapInPairs(head);
        oddEvenSeparation(head);
    }

    private static void oddEvenSeparation(LinkedListProblems head) {

        LinkedListProblems newHead = null;
        LinkedListProblems current = head;
        LinkedListProblems temp = head.next;
        while (true) {
            current.next = temp.next;
            if (newHead == null) {
                newHead = temp;
            }
            current = temp.next;
            temp.next = current.next;
            temp = current.next;
            if(temp == null || temp.next == null) {
                current.next = newHead;
                break;
            }
        }

    }

    private static void swapInPairs(LinkedListProblems head) {

        LinkedListProblems current = head;
        LinkedListProblems temp = current.next;

        LinkedListProblems newHead = null;
        LinkedListProblems prev = null;

        while (current != null && current.next != null) {
            current.next = temp.next;
            if (newHead == null) {
                newHead = temp;
            }
            temp.next = current;
            if (prev == null) {
                prev = current;
            } else {
                prev.next = temp;
                prev = current;
            }
            current = current.next;
            if (current != null && current.next != null) {
                temp = current.next;
            }

        }

    }
}

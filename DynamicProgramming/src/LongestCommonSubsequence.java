/**
 * Created by Anmol on 1/9/2016.
 */
public class LongestCommonSubsequence {

    public static void main(String[] args) {
        String str1 = "ABCDGH";
        String str2 = "AEDFHR";

        int l1 = str1.length();
        int l2 = str2.length();
        StringBuilder stringBuilder = new StringBuilder();
        int[][] result = new int[l1+1][l2+1];
        for(int i = 0; i<= l1;i++) {
            for(int j = 0; j<=l2; j++) {
                if(i==0||j==0) {
                    result[i][j] = 0;
                } else {
                    if(str1.charAt(i-1) == str2.charAt(j-1)) {
                        result[i][j] = 1 + result[i-1][j-1];
                    } else {
                        result[i][j] = Math.max(result[i-1][j], result[i][j-1]);
                    }
                }

            }

        }
        System.out.print(result[l1][l2]);
    }

}

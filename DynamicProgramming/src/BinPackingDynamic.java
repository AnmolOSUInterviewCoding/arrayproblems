/**
 * Created by Anmol on 9/29/2016.
 */
public class BinPackingDynamic {

    public static void main(String[] args) {
//        int[] arr = new int[]{4,8,1,4,2,1};
        int[] arr = new int[]{1,3,4,4,4,5,5,5};
        int sum = 24;
        int result[][] = new int[sum+1][arr.length+1];

        for(int i = 1;i<=sum;i++) {
            for(int j =1;j<=arr.length;j++) {
                result[i][j] = result[i][j-1];
                if(arr[j-1] == i) {
                    result[i][j] = i;
                } else if(i> arr[j-1]) {
                    result[i][j] = Math.max(result[i][j], arr[j-1] + result[i-arr[j-1]][j-1]);
                }

            }
        }
        System.out.print(0 + " ");
        for(int i = 0 ;i<arr.length;i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        for(int i = 0; i<result.length;i++) {
            for (int j = 0;j<result[0].length;j++) {
                System.out.print(result[i][j] + " ");
            }
            System.out.println();
        }
        int i = result[sum][arr.length];
        int j = arr.length;
        while (result[i][j]!=0) {
            if(result[i][j] > result[i][j-1] ) {
                if(result[i][j] > result[i-1][j]) {
                    System.out.print(arr[j-1] +" ");
                    i = i - arr[j-1];
                } else {
                    i--;
                }
            } else {
                j--;
            }
        }
    }
}

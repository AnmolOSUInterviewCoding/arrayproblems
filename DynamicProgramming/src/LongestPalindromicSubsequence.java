/**
 * Created by Anmol on 1/19/2016.
 */
public class LongestPalindromicSubsequence {

    public static void main(String[] args) {
        String string = "aaaabbbagbdba";
        int len = string.length();
        char[] str = string.toCharArray();
        int result[][] = new int[len][len];

        for(int i = 0; i< len;i++) {
            result[i][i] = 1;
        }

        for(int l = 2; l<=len;l++) {
            for(int i = 0; i<=len -  l;i++) {
                int j = i+l-1;
                if(str[i] == str[j] && l ==2) {
                    result[i][j] = 2;
                } else if(str[i]==str[j]) {
                    result[i][j] = 2 + result[i+1][j-1];
                } else{
                    result[i][j] = Math.max(result[i+1][j], result[i][j-1]);
                }
            }
        }
        System.out.println(result[0][len-1]);

    }

}

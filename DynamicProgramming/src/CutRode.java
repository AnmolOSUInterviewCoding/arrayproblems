/**
 * Created by Anmol on 1/10/2016.
 */
public class CutRode {

    public static void main(String[] args) {
//        int[] price = new int[]{ 1   ,5  , 8  , 9  ,10  ,17 , 17 , 20};
        int[] price = new int[]{  3   ,5   ,8   ,9 , 10  ,17  ,17  ,20};
        int length = 8;
        int[] result = new int[length+1];
        result[0] = 0;
        for(int i = 1; i<=length;i++) {
            int max = Integer.MIN_VALUE;
            for(int j = 0;j<price.length;j++) {
                if(i - (j+1) >=0){
                    max = Math.max(max, price[i-(j+1)] + result[j]);
                }
            }
            result[i] = max;
        }
        for(int i : result) {
            System.out.println(i);
        }
    }

}

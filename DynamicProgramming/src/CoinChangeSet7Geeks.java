/**
 * Created by Anmol on 1/28/2016.
 */
public class CoinChangeSet7Geeks {

    public static void main(String[] args) {
        int sum = 4;
        int coins[] = new int[]{1,2,3};

        int[][] result = new int[sum+1][coins.length];
        for(int i = 0;i<coins.length;i++) {
            result[0][i] = 1;
        }
        for(int i = 1; i<=sum;i++) {
            for(int j =0;j<coins.length;j++) {
                if(i>= coins[j]) {
                    int including = result[i - coins[j]][j];
                    int excluding = j>0? result[i][j-1]:0;
                    result[i][j] = including + excluding;
                }
            }
        }
        for(int i = 0; i<result.length;i++) {
            for(int j = 0; j<result[0].length;j++) {
                System.out.print(result[i][j] +" ");
            }
            System.out.print("\n");
        }
        /*int arr[] = new int[]{1, 3, 1, 4, 2, 5};

        int x = 7;
        int d = 3;*/
//        int[] arr = new int[]{101, 201, 301, 400, 500, 800, 45, 78, 123, 890, 600, 700};
        /*int[] arr = new int[]{175, 201, 301, 400, 500, 800, 45, 78, 1, 890, 600, 700};
        int x = 900;
        int d = 100;
//
        int Reach[] = new int[x + 1];
        int currentPosition = 0;
        for(int j=0; j<=d; j++) {
            if(j > x)
                continue;
                Reach[ j] = d;
        }
        for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j <= d; j++) {
                    if (arr[i] + j > x)
                        continue;
                    if (Reach[arr[i] + j] < arr[i] + d)
                        Reach[arr[i] + j] = arr[i] + d;
                }

                while (currentPosition <=x && Reach[currentPosition] > currentPosition)
                    currentPosition = Reach[currentPosition];

                if (currentPosition >= x) {
                    System.out.print(i);
                    return;
                }


        }*/

    }

}

/**
 * Created by Anmol on 2/7/2016.
 */
public class LongestBitonicSequence {

    public static void main(String[] args) {
        int[] arr = new int[] {0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5,
                13, 3, 11, 7, 15};

        int[] increasingSeq = new int[arr.length];
        increasingSeq[0] = 1;
        for(int i = 1; i<arr.length;i++) {
            for(int j =0; j<i;j++) {
                if(arr[i] > arr[j] && increasingSeq[i] < 1+ increasingSeq[j]) {
                    increasingSeq[i] = 1 + increasingSeq[j];
                }
            }
        }
        int[] decreasingSeq = new int[arr.length];
        decreasingSeq[arr.length-1] = 1;
        for(int i = arr.length-2; i>=0;i--) {
            for(int j = arr.length-1;j> i;j--) {
                if(arr[j] < arr[i] && decreasingSeq[i] < 1+ decreasingSeq[j]) {
                    decreasingSeq[i] = 1 + decreasingSeq[j];
                }
            }
        }
        System.out.println("Inceasing Seq: ");
        for(int i : increasingSeq) {
            System.out.print(i + " ");
        }
        System.out.println("Decreasing Seq: ");
        for(int i : decreasingSeq) {
            System.out.print(i + " ");
        }
        int max = decreasingSeq[0] + increasingSeq[0] - 1;
        for (int i = 1; i < arr.length; i++)
            if (decreasingSeq[i] + increasingSeq[i] - 1 > max)
                max = decreasingSeq[i] + increasingSeq[i] - 1;
        System.out.println(max);
    }
}

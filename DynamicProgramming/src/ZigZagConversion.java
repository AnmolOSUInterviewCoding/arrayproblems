/**
 * Created by Anmol on 2/16/2016.
 */
public class ZigZagConversion {

    public static void main(String[] args) {
        String str = "GEEKSFORGEEKS";
        int n = 2;
        String[] strings = new String[n];
        boolean down = true;
        int row = 0;
        for(int i =0;i<str.length();i++) {
            if(strings[row] == null) {
                strings[row] = String.valueOf(str.charAt(i));
            } else {
                strings[row] = strings[row].concat(String.valueOf(str.charAt(i)));
            }
            if(row == n-1) down =false;

            if(row == 0) down = true;

            row = down ? row+1 : row-1;
        }
        String s = "";
        for(String string : strings) {
           s= s.concat(string);
        }
        System.out.print(s);
    }
}

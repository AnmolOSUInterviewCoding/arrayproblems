import java.util.Arrays;

/**
 * Created by Anmol on 2/9/2016.
 */
public class WeightedJobScheduling {

    static class Job implements Comparable<Job>{
        int startTime;
        int endTime;
        int profitValue;

        Job(int startTime, int endTime, int profitValue) {
            this.startTime = startTime;
            this.endTime = endTime;
            this.profitValue = profitValue;
        }
        // Sort the jobs according to increasing order of end time.
        @Override
        public int compareTo(Job o) {
            //ascending.
            if(this.endTime > o.endTime) {
                return 1;

            }
            return -1;

        }
    }

    public static void main(String[] args) {
        /*Job[] jobs = new Job[]{new Job(1,3,5), new Job(2,5,6), new Job(4,6,5), new Job(6,7,4),
                                new Job(5,8,11), new Job(7,9,2)};*/
        Job[] jobs = new Job[]{new Job(3, 10, 20), new Job(1, 2, 50), new Job(6, 19, 100), new Job(2, 100, 200)};
        Arrays.sort(jobs);

        getMaxProfit(jobs);


    }

    private static void getMaxProfit(Job[] jobs) {
        int[] profit = new int[jobs.length];

        for(int i =0; i<jobs.length;i++) {
            profit[i] = jobs[i].profitValue;
        }

        for(int i =1;i< profit.length;i++) {
            for(int j = 0;j<i;j++) {
                /****if two jobs are not overlapping then add the profit whatever we have till j(from other processes) +
                the profit by scheduling process i.****/
                if(jobs[i].startTime >= jobs[j].endTime) {
                    int sumProfit = jobs[i].profitValue + profit[j];
                    if(sumProfit> profit[i]) {
                        profit[i] = sumProfit;
                    }
                }
            }
        }
        int max = Integer.MIN_VALUE;
        for(int i : profit) {
            if(i > max) {
                max = i;
            }
            System.out.print(i + " ");
        }
        System.out.print("\nMax Profit: " + max);
    }
}

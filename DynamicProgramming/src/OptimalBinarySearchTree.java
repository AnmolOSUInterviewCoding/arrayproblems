/**
 * Created by Anmol on 2/9/2016.
 */
public class OptimalBinarySearchTree {

    public static void main(String[] args) {
        int[] freq = new int[]{4,2,3,6};
        int[] bst = new int[]{10,12,16,21};

        int[][] output = new int[freq.length][freq.length];

        for(int i = 0; i<freq.length;i++) {
            output[i][i] = freq[i];
        }

        for(int l = 2; l<=freq.length;l++) {
            for(int i = 0; i<=freq.length -l;i++){
                int j = i + l -1;
                int freqSum = getSum(freq, i, j);
                output[i][j] = Integer.MAX_VALUE;
                for(int k =i;k<=j;k++) {

                    int val = freqSum + (k-1 < i ? 0 : output[i][k-1]) +
                            (k+1 > j ? 0 : output[k+1][j]) ;
                    if(val < output[i][j]){
                        output[i][j] = val;
                    }
                }

            }
        }
    }

    private static int getSum(int[] freq, int i, int j) {
        int sum = 0;
        for(int x = i;x<=j;x++) {
            sum += freq[x];
        }
        return sum;
    }
}

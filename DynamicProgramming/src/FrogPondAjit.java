/**
 * Created by Anmol on 2/8/2016.
 */
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

/**
 * @author Ajit Sahoo
 */

public class FrogPondAjit {

    int[] leaf;
    int n;
    int d;
    int x;

    public FrogPondAjit(int[] leaf, int n, int d, int x) {
        this.leaf = leaf;
        this.n = n;
        this.d = d;
        this.x = x;
    }

    public void solution(){

        if (d >= x) {
            System.out.println("Frog reaches in 0 sec!");
        }


        for (int i = 0; i < n; i++)  {

            Set currLeaves = new HashSet<Integer>();
            for (int j = 0; j<=i; j++) {
                currLeaves.add(leaf[j]);
            }
            System.out.println("set of leaf position: " + currLeaves);
            //shall I sort the set?
            findSolution(currLeaves.iterator(), i, 0, d, x);

        }



    }

    private void findSolution(Iterator<Integer> iterator, int nSec, int currPos, int d, int endPos) {

        if (currPos + d >= endPos) {
            System.out.println("frog reached. Took " + nSec + "sec.");
            return;
        }

        if (iterator.hasNext()) {
            int next = iterator.next();
            if (canGoToNextLeaf(currPos, d, next)) {
                findSolution(iterator, nSec, next, d, endPos);
            }

        }

    }

    private boolean canGoToNextLeaf(int currPos, int d, int next) {return currPos + d >= next;}

    public static void main(String[] args) {
        int leaf[] = new int[10];
        int x;
        int d;
        int n;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter X = ");
        x = scanner.nextInt();

        System.out.println("Enter D = ");
        d = scanner.nextInt();

        System.out.println("Enter no. of leaves = ");
        n = scanner.nextInt();

        for (int i =0 ;i < n; i++) {
            System.out.println("Leaf[" + i + "] = ");
            leaf[i] = scanner.nextInt();
        }

        System.out.println("Input Received !!");

        FrogPondAjit frogPond = new FrogPondAjit(leaf, n, d, x);
        frogPond.solution();
    }

}
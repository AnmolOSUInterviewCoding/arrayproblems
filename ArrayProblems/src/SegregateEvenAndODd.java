/**
 * Created by Anmol on 2/3/2016.
 */
public class SegregateEvenAndODd {
    public static void main(String[] args) {
        int[] arr = new int[]{12, 34, 45, 9, 8, 90, 3};

        int i = 0,j = 0;
        while(j<arr.length) {
            if(arr[j]%2!=0) {

                j++;
            } else {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j++;
            }
        }
        for(int k: arr) {
            System.out.print(k +" ");
        }
    }

}

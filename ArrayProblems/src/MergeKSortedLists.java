/**
 * Created by Anmol on 2/18/2016.
 */
public class MergeKSortedLists {
    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }
    public static void main(String[] args) {
        LinkedList[] lists = new LinkedList[4];
        LinkedList head = new LinkedList(12);
        head.next = new LinkedList(15);
        lists[0] = head;

        LinkedList head1 = new LinkedList(2);

        head1.next = new LinkedList(4);
        lists[1] = head1;

        LinkedList head2 = new LinkedList(3);

        head2.next = new LinkedList(5);
        lists[2] = head2;
        LinkedList head3 = new LinkedList(11);

        head3.next = new LinkedList(16);
        lists[3] = head3;
        sortKLists(lists);

    }

    private static void sortKLists(LinkedList[] lists) {
        LinkedList merged = sortKLists(lists, 0, lists.length-1);
    }

    private static LinkedList sortKLists(LinkedList[] lists, int start, int end) {
        if(start < end) {

            int mid = (start + end) / 2;
            LinkedList head1 = sortKLists(lists, start, mid );
            LinkedList head2 = sortKLists(lists, mid + 1, end);
            LinkedList node = merge(head1, head2);
            return node;
        } else {
            return lists[start];
        }
    }

    private static LinkedList merge(LinkedList head1, LinkedList head2) {
        if(head2 == null) return head1;
        if(head1 == null) return head2;
        LinkedList node;
        if(head1.val < head2.val) {
            node = new LinkedList(head1.val);
            node.next = merge(head1.next, head2);
        } else{
            node = new LinkedList(head2.val);
            node.next = merge(head1, head2.next);
        }
        return node;
    }
}

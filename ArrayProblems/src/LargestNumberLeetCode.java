import java.util.*;

/**
 * Created by Anmol on 2/16/2016.
 */
public class LargestNumberLeetCode {

    public static void main(String[] args) {
        int[] arr = new int[]{3,30,34,5,9, 8};

        String[] strings = new String[arr.length];
        for(int i = 0;i<strings.length;i++) {
            strings[i] = String.valueOf(arr[i]);
        }
        Arrays.sort(strings, new Comparator<String>(){
            public int compare(String s1, String s2){
                String leftRight = s1+s2;
                String rightLeft = s2+s1;
                return -leftRight.compareTo(rightLeft);

            }
        });
        StringBuilder sb = new StringBuilder();
        for(String s: strings){
            sb.append(s);
        }

        while(sb.charAt(0)=='0' && sb.length()>1){
            sb.deleteCharAt(0);
        }


    }
}

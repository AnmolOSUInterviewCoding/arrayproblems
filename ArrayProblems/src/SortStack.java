import java.util.Stack;

/**
 * Created by Anmol on 2/13/2016.
 */
public class SortStack {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();

        stack.push(4);
        stack.push(2);
        stack.push(6);
        stack.push(1);
        stack.push(3);
        stack.push(5);

        sort(stack);
        for(int i : stack) {
            System.out.print(i +" ");
        }
    }

    private static void sort(Stack<Integer> stack) {
        if(!stack.empty()) {
            int temp = stack.pop();
            sort(stack);

            insertLast(stack, temp);
        }
    }

    private static void insertLast(Stack<Integer> stack, int temp) {
        if(stack.isEmpty()) stack.push(temp);

        else {
            if(stack.peek() > temp) {
                int top = stack.pop();
                insertLast(stack, temp);
                stack.push(top);
            } else {
                stack.push(temp);
            }
        }
    }
}

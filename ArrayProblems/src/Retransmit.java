import java.io.*;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by Anmol on 4/19/2016.
 */
public class Retransmit {

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = null;
        try {
             bufferedReader = new BufferedReader(new FileReader("C:\\Users\\Anmol\\ArrayProblems\\src\\tcp_test_output"));
        } catch (FileNotFoundException e) {
            System.out.println("File Not found!!");
            System.exit(-1);
        }

        PrintWriter fw = new PrintWriter(new FileWriter("C:\\Users\\Anmol\\ArrayProblems\\src\\packets.txt"));
        PrintWriter printWriter = new PrintWriter(new FileWriter("C:\\Users\\Anmol\\ArrayProblems\\src\\retransmit.txt"));
        String line;
        CharSequence sequence1 = "[node 0]";
        CharSequence sequence2 = "sending seq";
        ArrayList<Integer> packetSeq = new ArrayList<Integer>();
        while ((line = bufferedReader.readLine())!=null) {
            if (line.contains(sequence1) && line.contains(sequence2)) {
                String[] split = line.split(" ");
                int packetNumber = Integer.parseInt(split[7]);
                String timePacket = split[0] + "\t" + split[7] +"\n";
                if(!packetSeq.contains(packetNumber)) {
                    fw.write(timePacket);
                    packetSeq.add(packetNumber);
                } else {
                    printWriter.write(timePacket);
                }
            }
        }

        fw.close();
        printWriter.close();
    }
}

/**
 * Created by Anmol on 2/3/2016.
 */
public class FindTwoNonRepeatingCharacters {

    public static void main(String[] args) {
        int[] arr = new int[]{4, 2, 4, 5, 2, 3, 1};

        for(int i = 0; i<arr.length;i++) {
            if(arr[Math.abs(arr[i])] < 0) {
                System.out.print(Math.abs(arr[i]) +" ");
            } else {
                arr[Math.abs(arr[i])] = -arr[Math.abs(arr[i])];
            }
        }
    }
}

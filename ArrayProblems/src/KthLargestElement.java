import java.util.Arrays;

/**
 * Created by Anmol on 2/2/2016.
 */
public class KthLargestElement {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 23, 12, 9, 30, 2, 50};
        int k = 2;
        Arrays.sort(arr);
        System.out.print(arr[arr.length -k]);
    }
}

/**
 * Created by Anmol on 2/23/2016.
 */
public class RepeatingAndMissing {

    public static void main(String[] args) {
        int[] arr = new int[]{4, 3, 6, 2, 1, 1};

        for(int i = 0; i<arr.length;i++) {
            if(arr[Math.abs(arr[i])-1]>0) {
                arr[Math.abs(arr[i])-1] = - arr[Math.abs(arr[i])-1];
            } else {
                System.out.print("Repeating: "+Math.abs(arr[i])+" ");
            }
        }

        for(int i = 0;i<arr.length;i++) {
            if(arr[i]>0) {
                System.out.print("Missing: " + (i+1));
            }
        }

    }

}

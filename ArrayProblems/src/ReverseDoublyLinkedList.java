/**
 * Created by Anmol on 2/18/2016.
 */
public class ReverseDoublyLinkedList {
    static DoubleLinkedList head;
    static class DoubleLinkedList {
        int val;
        DoubleLinkedList next;
        DoubleLinkedList prev;

        DoubleLinkedList(int val) {
            this.val = val;
            this.next = null;
            this.prev = null;
        }
    }

    private static void insertAtBeginning(int val) {
        if(head ==null) {
            head = new DoubleLinkedList(val);
        } else {
            DoubleLinkedList temp = new DoubleLinkedList(val);
            temp.next = head;
            head.prev = temp;
            head = temp;
        }
    }

    public static void main(String[] agrs) {
        insertAtBeginning(4);
        insertAtBeginning(3);
        insertAtBeginning(2);
        insertAtBeginning(1);

        reverseDLL();
    }

    private static void reverseDLL() {
        DoubleLinkedList temp = null, curr= head;

        while (curr!=null) {
           temp = curr.prev;
           curr.prev = curr.next;
            curr.next = temp;
            curr = curr.prev;
        }
        head = temp.prev;
    }
}

import java.util.Arrays;

/**
 * Created by Anmol on 9/17/2016.
 */
public class ThreeSumClosest {

    public static void main(String args[]) {
        ThreeSumClosest threeSumClosest = new ThreeSumClosest();
        int[] arr = new int[]{1,2,4,8,16,32,64,128};
        int target = 82;
        int result = threeSumClosest.getThreeSumClosest(arr, target);
    }

    private int getThreeSumClosest(int[] nums, int target) {
        int minDiff = Integer.MAX_VALUE;
        Arrays.sort(nums);
        int j;
        int result = 0;
        for(int i = 0; i<=nums.length-3;i++) {
            j = i+1;
            int k = nums.length-1;
            while(j<k) {
                int sum = nums[i]+nums[j]+nums[k];
                if(Math.abs(sum-target)<=minDiff) {
                    result = sum;
                    minDiff = Math.abs(sum-target);
                }
                if(sum > target){
                    k--;
                } else{
                    j++;
                }
            }
        }
        return result;
    }
}

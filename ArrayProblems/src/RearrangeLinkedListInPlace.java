/**
 * Created by Anmol on 2/23/2016.
 */
public class RearrangeLinkedListInPlace {
    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }

    public static void main(String[] args) {
        LinkedList head = new LinkedList(1);
        head.next = new LinkedList(2);
        head.next.next = new LinkedList(3);
        head.next.next.next = new LinkedList(4);
        head.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next = new LinkedList(6);
        head.next.next.next.next.next.next = new LinkedList(7);

        rearrangeLinkedListInPlace(head);
    }

    private static void rearrangeLinkedListInPlace(LinkedList head) {
        LinkedList slow = head, fast = head;

        while (fast!=null) {
            slow = slow.next;
            fast = fast.next;

            if(fast == null) break;

            fast = fast.next;

        }

        LinkedList head2 = slow;
        head2 = reverseLinkedList(head2);
        LinkedList temp1 = head, temp2 = head, print = head;
        while (head2!=null && head!=slow) {
            temp1 = head.next;
            temp2 = head2.next;
            head.next = head2;
            head2.next = temp1;
            head = temp1;
            head2 = temp2;
        }
        if(head!=null)
        head.next = null;

        while (print!=null) {
            System.out.print(print.val +" ");
            print = print.next;
        }

    }

    private static LinkedList reverseLinkedList(LinkedList head2) {
        LinkedList curr = head2, temp = head2, prev = null;
        while (curr!=null) {
            curr = curr.next;
            temp.next = prev;
            prev = temp;
            temp = curr;
        }
        return prev;
    }
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Anmol on 2/20/2016.
 */
public class FourSumLeetCode {
    public static void main(String[] args) {
//        int[] nums = new int[]{-1, 0, 0, 0, 0, 0, 0, 0, 1};
        int[] nums = new int[]{1, 0, -1, 0, -2, 2, 4, 3, -3};
        List<List<Integer>> lists = fourSum(nums, 0);
    }

    private static List<List<Integer>> fourSum(int[] nums, int target) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<List<Integer>>();

        for (int i = 0; i < nums.length - 3; i++) {
            for (int j = i + 1; j < nums.length - 2; j++) {
                int k = j + 1;
                int end = nums.length - 1;
                while (k < end) {
                    int sum = nums[i] + nums[j] + nums[k] + nums[end];

                    if (sum < target) {
                        k++;
                    } else if (sum > target) {
                        end--;
                    } else {
                        List<Integer> numbers = new ArrayList<Integer>();
                        numbers.add(nums[i]);
                        numbers.add(nums[j]);
                        numbers.add(nums[k]);
                        numbers.add(nums[end]);
                        if (!result.contains(numbers)) result.add(numbers);
                        do {
                            k++;
                        } while (k < end && nums[k] == nums[k - 1]);
                        do {
                            end--;
                        } while (k < end && nums[end] == nums[end + 1]);

                    }
                }
            }
        }


        return result;
    }
}

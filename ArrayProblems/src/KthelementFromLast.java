/**
 * Created by Anmol on 2/13/2016.
 */
public class KthelementFromLast {
    public static void main(String[] args) {
        RemoveDuplicatesFromLinkedList.LinkedList head = RemoveDuplicatesFromLinkedList.getLinkedList();
        getKthElementFromLast(head, 5);
    }

    private static void getKthElementFromLast(RemoveDuplicatesFromLinkedList.LinkedList head, int kth) {
        int length = 0;
        RemoveDuplicatesFromLinkedList.LinkedList temp = head;
        while(temp!=null) {
            length++;
            temp = temp.next;
        }
        temp = head;
        for(int i =0; i<length-kth;i++ ) {
            temp = temp.next;
        }
        System.out.print(temp.val);
    }
}

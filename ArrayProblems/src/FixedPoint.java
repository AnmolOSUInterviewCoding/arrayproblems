/**
 * Created by Anmol on 2/12/2016.
 */
public class FixedPoint {

    public static void main(String[] args) {
        int[] arr = new int[]{-10, -5, 0, 3, 7};
//        int[] arr = new int[]{0, 2, 5, 8, 17};
//        int[] arr = new int[]{-10, -5, 3, 4, 7, 9};

        int start = 0;
        int end = arr.length-1;
        int mid = -1;
        while(start< end) {
             mid = (start + end)/2;

            if(arr[mid] == mid) {
                System.out.print("Fixed Point found: " + mid + " ");
                break;
            }
            if(arr[mid]< mid) {
                start = mid+1;
            } else {
                end = mid -1;
            }
        }
        if(arr[mid]!=mid) {
            System.out.print("Index not found");
        }
    }
}

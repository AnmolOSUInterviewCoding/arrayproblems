/**
 * Created by Anmol on 9/17/2016.
 */
public class RemoveDuplicatesFromSortedArray {
    public static void main(String[] args) {
        RemoveDuplicatesFromSortedArray removeDuplicatesFromSortedArray = new RemoveDuplicatesFromSortedArray();
        int[] nums = new int[]{1,1,1,2,2,3,3,4,5,6,7,7,7,8};
        int result = removeDuplicatesFromSortedArray.removeDuplicates(nums);
    }

    private int removeDuplicates(int[] nums) {
        int i =0;
        for(int j = 1; j<nums.length;j++){
            if(nums[j]!=nums[i]) {
                i++;
                nums[i] = nums[j];
            }
        }
        for(int k : nums){
            System.out.print(k +" ");
        }
        System.out.print("i: "+i);
        return i;
    }
}

/**
 * Created by Anmol on 2/3/2016.
 */
public class MaxProductSubarray {

    public static void main(String[] args) {
        int arr[] = new int[]{2,3,-2,4, -1};

        int maxProductTillHere = arr[0];
        int minProductTillHere = arr[0];
        int totalMaxProduct = arr[0];
        for(int i= 1; i< arr.length;i++) {
            int tempMax = maxProductTillHere;
            int tempMin = minProductTillHere;
            maxProductTillHere = Math.max(Math.max(tempMax* arr[i], tempMin*arr[i]), arr[i]);
            minProductTillHere = Math.min(Math.min(tempMax * arr[i], tempMin * arr[i]), arr[i]);
            totalMaxProduct = Math.max(maxProductTillHere, totalMaxProduct);
        }
        System.out.print(totalMaxProduct);
    }

}

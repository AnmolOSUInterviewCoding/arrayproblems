/**
 * Created by Anmol on 2/17/2016.
 */
public class RotateListLEetCode {

    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }
    public static void main(String[] args) {
        LinkedList head = new LinkedList(1);
        head.next = new LinkedList(2);
        head.next.next = new LinkedList(3);
        head.next.next.next = new LinkedList(4);
        head.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next = new LinkedList(6);
        head.next.next.next.next.next.next = new LinkedList(7);

        LinkedList newHead =rotateList(head, 1);
        while (newHead!=null) {
            System.out.print(newHead.val +" ");
            newHead = newHead.next;
        }
    }

    private static LinkedList rotateList(LinkedList head, int m) {
        int i = 0;
        LinkedList end = head;
        LinkedList node = head;
        int len = 0;
        while (node!=null) {
            node = node.next;
            len++;
        }
        System.out.print("length is: "+ len+" ");
        while(i <len- m-1) {
            end = end.next;
            i++;
        }
        LinkedList newHead = end.next;
        end.next = null;
        LinkedList temp = newHead, prev= null;

        while (temp!=null) {
            prev = temp;
            temp = temp.next;
        }
        prev.next = head;
        return newHead;
    }
}

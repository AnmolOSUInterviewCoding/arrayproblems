/**
 * Created by Anmol on 2/14/2016.
 */
public class ArrayRotation {

    public static void main(String[] args) {
        int arr[] = new int[]{1,2,3,4,5,6,7};

        int rotate = 2;

        rotate(arr, rotate);

    }

    private static void rotate(int[] arr, int rotate) {
        for (int i = 0;i<rotate;i++) {
            rotateOneByOne(arr);
        }
        for(int i: arr)
        System.out.print(i +" ");
    }

    private static void rotateOneByOne(int[] arr) {
        int temp = arr[0];
        for(int i = 0;i<arr.length-1;i++) {
            arr[i] = arr[i+1];
        }
        arr[arr.length-1] = temp;
    }
}

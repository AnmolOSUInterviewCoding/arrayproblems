/**
 * Created by Anmol on 3/20/2016.
 */
public class MaxDifferenceBetweenArrayElements {

    public static void main(String[] args) {
        int[] arr = new int[]{80, 2, 6, 3, 10};

        int max  = getMaxDifference(arr);
        System.out.print(max+" ");
    }

    private static int getMaxDifference(int[] arr) {
        if(arr.length <=1) {
            return -1;
        }

        int max_diff = arr[1] - arr[0];
        int min_element = arr[0];
        for(int i = 1; i < arr.length; i++)
        {
            if (arr[i] - min_element > max_diff)
                max_diff = arr[i] - min_element;
            if (arr[i] < min_element)
                min_element = arr[i];
        }
        return max_diff;
    }
}

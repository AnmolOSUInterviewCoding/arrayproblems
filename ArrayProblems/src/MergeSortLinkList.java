import java.util.List;

/**
 * Created by Anmol on 2/13/2016.
 */
public class MergeSortLinkList {
    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }
    public static void main(String[] args) {
        LinkedList head = new LinkedList(1);
        head.next = new LinkedList(6);
        head.next.next = new LinkedList(8);
        head.next.next.next = new LinkedList(2);
        head.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next = new LinkedList(4);
        head.next.next.next.next.next.next = new LinkedList(3);
        head.next.next.next.next.next.next.next = new LinkedList(10);
        head.next.next.next.next.next.next.next.next = new LinkedList(9);
        head.next.next.next.next.next.next.next.next.next = new LinkedList(7);


        LinkedList temp = sort(head);;
        while (temp!=null) {
            System.out.print(temp.val+" ");
            temp = temp.next;
        }
    }

    private static LinkedList sort(LinkedList head) {
        if(head == null || head.next == null) return head;
        int len = findLength(head);

        int mid = len/2;
        LinkedList l1 = head,l2 = null;
        LinkedList temp = l1;
        int tillHalf = 1;
        while(temp!=null) {
            if(tillHalf == mid) {
                l2 = temp.next;
                temp.next = null;
                break;
            }
            tillHalf++;
            temp = temp.next;

        }
        LinkedList h1 = sort(l1);
        LinkedList h2 = sort(l2);

        return mergeSort(h1,h2);
    }

    private static LinkedList mergeSort(LinkedList h1, LinkedList h2) {
        if(h1 == null) return h2;
        if(h2 == null) return h1;

        LinkedList node;
        if(h1.val < h2.val) {
            node = new LinkedList(h1.val);
            node.next = mergeSort(h1.next, h2);
        } else {
            node = new LinkedList(h2.val);
            node.next = mergeSort(h1, h2.next);
        }
        return node;
    }

    private static int findLength(LinkedList head) {
        LinkedList temp = head;
        int len = 0;
        while (temp!=null) {
            len++;
            temp = temp.next;
        }
        return len;
    }
}

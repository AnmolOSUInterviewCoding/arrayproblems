/**
 * Created by Anmol on 2/4/2016.
 */
public class DecodeWaysLeetCode {
    public static void main(String[] args) {
        String s = "123";

        int[] t = new int[s.length()+1];
        t[0] = 1;
        //if(s.charAt(0)!='0')
        if(isValid(s.substring(0,1)))
            t[1]=1;
        else
            t[1]=0;

        for(int i=2; i<=s.length(); i++){
            if(isValid(s.substring(i-1,i))){
                t[i]+=t[i-1];
            }

            if(isValid(s.substring(i-2,i))){
                t[i]+=t[i-2];
            }
        }

    }



    public static boolean isValid(String s){
        if(s.charAt(0)=='0')
            return false;
        int value = Integer.parseInt(s);
        return value>=1&&value<=26;
    }
}

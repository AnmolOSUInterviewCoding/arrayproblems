import java.util.*;

/**
 * Created by Anmol on 10/22/2016.
 */
public class Top5Marks {

    class Result {
        public int id;
        public int marks;
        Result(int id, int marks) {
            this.id = id;
            this.marks = marks;
        }
    }

    public static void main(String[] args) {
        Top5Marks marks = new Top5Marks();
        List<Result> students = marks.getStudentList();
        Map<Integer, Double> result = marks.getTop5Marks(students);
    }

    private Map<Integer, Double> getTop5Marks(List<Result> students) {
        Map<Integer, Student> map = new HashMap<>();
        List<Student> list = new ArrayList<>();
//        PriorityQueue<Student> priorityQueue = new PriorityQueue<>();
        for(Result student : students) {
            if(!list.contains(student.id)) {
                list.add(new Student(student.id));
            }
        }
        for(Result result : students) {
            Student student = list.get(result.id);
            student.addMarks(result.marks);
        }
//        System.out.println(priorityQueue);
        for(Student student : list) {
            /*Collections.sort(student.marks, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return Double.compare(o2, o1);
                }
            });*/
            Collections.sort(student.marks);
            Collections.reverse(student.marks);
            for(Integer i : student.marks) {
                System.out.print(i +" ");
            }
            System.out.println();
        }


        return null;
    }

    private List<Result> getStudentList() {
        List<Result> results = new ArrayList<>();
        results.add(new Result(1,40));
        results.add(new Result(1,50));
        results.add(new Result(1,60));
        results.add(new Result(1,70));
        results.add(new Result(1,80));
        results.add(new Result(2, 130));

        results.add(new Result(1,90));

        results.add(new Result(2, 10));
        results.add(new Result(1,120));
        results.add(new Result(1,150));
        return results;
    }

}

class Student  {
    int id;
    List<Integer> marks = new ArrayList<>();

    Student(int id) {
        this.id = id;

    }
    public void addMarks(int mark) {
        marks.add(mark);
    }


    /*@Override
    public int compareTo(Student o1) {
        if(o1.id == this.id) {
            return Double.valueOf(o1.marks).compareTo((double) this.marks);
        } else {
            return Double.valueOf(o1.id).compareTo((double) this.id);
        }
    }

    @Override
    public String toString() {
        return "[id: " + id + " marks: " + marks +" ]";
    }*/
}


/**
 * Created by Anmol on 2/2/2016.
 */
public class BuyAndSellStocks2LeetCode {

    public static void main(String[] args) {
        int prices[] = new int[]{3,4,1,5,6,3,9};
        int profit = 0;
        int totalProfit = 0;
        int buyPrice = prices[0];
        for(int i = 1; i< prices.length;i++) {
            if(prices[i]> buyPrice && profit< (prices[i]-buyPrice)) {
                profit = prices[i] - buyPrice;
            } else if(prices[i-1]> prices[i]) {
                totalProfit+= profit;
                profit = 0;
                buyPrice = prices[i];
            }
        }
        totalProfit += profit;
        System.out.print(totalProfit);
    }

}

import java.util.Scanner;

/**
 * Created by Anmol on 3/20/2016.
 */
public class FindCommnets {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberOfLines = sc.nextInt();
        CharSequence singleLine = "//";
        CharSequence multiLineStart = "/*";
        CharSequence multiLineEnd = "*/";
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i< numberOfLines;i++) {
            String line = sc.nextLine();
            if(line.contains(singleLine)) {
                int index = line.indexOf("//");
                stringBuilder.append(line.substring(index));
                stringBuilder.append("\n");
            } else if(line.contains(multiLineStart)) {
                int start = line.indexOf("/*");
                if(line.contains(multiLineEnd)) {
                    stringBuilder.append(line.substring(start, line.indexOf("*/")));
                    stringBuilder.append("*/");
                    stringBuilder.append("\n");
                } else {
                    stringBuilder.append(line.substring(start));
                    stringBuilder.append("\n");
                    while (sc.hasNext()) {
                        String nextLine = sc.nextLine();
                        if(nextLine.contains(multiLineEnd)) {
                            stringBuilder.append(nextLine.substring(0, nextLine.indexOf("*/")));
                            stringBuilder.append("*/");
                            stringBuilder.append("\n");
                            break;
                        } else {
                            stringBuilder.append(nextLine);
                            stringBuilder.append("\n");
                        }
                    }
                }

            }
        }
        System.out.print(stringBuilder);
        sc.close();
    }
}

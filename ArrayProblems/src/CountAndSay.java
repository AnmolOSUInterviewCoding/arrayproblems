/**
 * Created by Anmol on 2/4/2016.
 */
public class CountAndSay {

    public static void main(String[] java) {
        int n = 5;
        String result = "10";
        int i = 1;
        char c = '1';
        String k = result.substring(0, 2);
        while (i < n) {
            StringBuilder sb = new StringBuilder();
            int count = 1;
            for (int j = 1; j < result.length(); j++) {
                if (result.charAt(j) == result.charAt(j - 1)) {
                    count++;
                } else {
                    sb.append(count);
                    sb.append(result.charAt(j - 1));
                    count = 1;
                }
            }

            sb.append(count);
            sb.append(result.charAt(result.length() - 1));
            result = sb.toString();
            i++;
        }
    }
}

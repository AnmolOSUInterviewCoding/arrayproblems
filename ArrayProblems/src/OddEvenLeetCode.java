/**
 * Created by Anmol on 2/26/2016.
 */
public class OddEvenLeetCode {
    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }
    public static void main(String[] args) {
        LinkedList head = new LinkedList(1);
        head.next = new LinkedList(2);
        head.next.next = new LinkedList(3);
        head.next.next.next = new LinkedList(4);
        head.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next = new LinkedList(6);
        head.next.next.next.next.next.next = new LinkedList(7);
        head.next.next.next.next.next.next.next = new LinkedList(8);

        oddEvenList(head);
        String s1 = new String("Hello");
        String s2 = new String("bhai");
        String s3 = new String();
        s3 = s1+s2;
//        s3 = s1-s2;
        System.out.print("Final: " + s3);

    }
    public static LinkedList oddEvenList(LinkedList head) {
        if(head == null|| head.next == null || head.next.next == null) return head;
        /*LinkedList newHead = null;
        LinkedList current = head;
        LinkedList temp = head.next;
        while (true) {
            current.next = temp.next;
            if (newHead == null) {
                newHead = temp;
            }
            current = temp.next;
            temp.next = current.next;
            temp = current.next;
            if(temp == null || temp.next == null) {
                current.next = newHead;
                return head;
            }
        }*/
        LinkedList odd = head, evenHead = head.next, even = head.next, prevOdd = null;
        while(odd!=null && even!=null) {
            odd.next = even.next;
            prevOdd = odd;
            odd = odd.next;
            if(odd!=null) {
                System.out.println("odd val: " + odd.val);
                even.next = odd.next;
                even = odd.next;
                System.out.println("even val: " + even.val);

            }
        }
        prevOdd.next = evenHead;
        return head;
    }
}

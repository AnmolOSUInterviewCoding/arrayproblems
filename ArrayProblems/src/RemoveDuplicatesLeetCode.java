import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anmol on 2/13/2016.
 */
public class RemoveDuplicatesLeetCode {

    static class LinkedList {
        int val;
        LinkedList next;

        LinkedList(int val) {
            this.val = val;
            this.next = null;
        }
    }

    public static void main(String[] args) {
        LinkedList head = new LinkedList(1);
        head.next = new LinkedList(1);
        head.next.next = new LinkedList(1);
        head.next.next.next = new LinkedList(2);
        head.next.next.next.next = new LinkedList(5);
        head.next.next.next.next.next = new LinkedList(4);
        head.next.next.next.next.next.next = new LinkedList(1);
        head.next.next.next.next.next.next.next = new LinkedList(1);
        head.next.next.next.next.next.next.next.next = new LinkedList(1);
        head.next.next.next.next.next.next.next.next.next = new LinkedList(5);

        LinkedList temp = removeDuplicates(head);
    }

    private static LinkedList removeDuplicates(LinkedList head) {
        LinkedList temp = head;
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        while (temp!=null) {
            if(map.containsKey(temp.val)) {
                map.put(temp.val, 2);
            } else {
                map.put(temp.val, 1);
            }
            temp = temp.next;
        }
        LinkedList newHead = null;
        LinkedList linkedList=null;
        temp = head;
        while (temp!=null) {
            if(map.get(temp.val) ==1) {
                if(newHead == null) {
                    newHead = new LinkedList(temp.val);
                    linkedList = newHead;
                } else {
                    linkedList.next = new LinkedList(temp.val);
                    linkedList = linkedList.next;
                }
            }
            temp = temp.next;
        }
        return  newHead;
    }
}

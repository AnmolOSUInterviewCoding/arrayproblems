import java.util.Stack;

/**
 * Created by Anmol on 2/13/2016.
 */
public class ReverseStackUsingStack {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);

        reverse(stack);
        for(int i : stack) {
            System.out.print(i +" ");
        }
    }

    private static void reverse(Stack<Integer> stack) {
        if(!stack.empty()) {
            int temp = stack.pop();
            reverse(stack);
//            stack.push(temp);
            insertLast(stack, temp);
        }
    }

    private static void insertLast(Stack<Integer> stack, int temp) {
        if(stack.isEmpty()) stack.push(temp);

        else {
            int top = stack.pop();
            insertLast(stack, temp);
            stack.push(top);
        }
    }
}
